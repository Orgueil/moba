﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sTurret : MonoBehaviour
{
    
    public float rayDistance;
    public float radius;
    public LayerMask Enemy;
    private Rigidbody rb;
    public bool attack;
    public int health;
    public int goldValue = 1000;
    private int xpValue = 1000;
    public void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Die();
        }
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        
        RaycastHit[] hitlist = Physics.SphereCastAll(transform.position, radius, Vector3.forward, rayDistance, Enemy);

        foreach (RaycastHit i in hitlist)
        {
            attack = true;
            Debug.Log ("Enemie détecter");
        }

        {
           // attack = false;
        }
    }

    void Die()
    {
        {
            butin.loot += goldValue;
            butin.xp += xpValue;
            Destroy(gameObject);
        }
    }

}
