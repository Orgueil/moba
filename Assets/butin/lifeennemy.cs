﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lifeennemy : MonoBehaviour
{
    public int life;
    public GameObject ennemy1;
    public int loot;
    public int experience;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            life--;
        }

        if (life == 0)
        {
            loot += 10;
            Destroy(ennemy1);
        }
    }
}
