﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lifeennemy2 : MonoBehaviour
{
    public int life2;
    public GameObject ennemy2;
    public int loot;
    public int experience;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            life2--;
        }

        if (life2 == 0)
        {
            loot += 20;
            Destroy(ennemy2);
        }
    }
}

