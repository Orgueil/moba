﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CompétenceClass
{
    public string compName;

    public int id;

    public float cd;

    public float baseCd;
}
