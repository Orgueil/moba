﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using UnityEngine;
using UnityEngine.UI;
using Component = UnityEngine.Component;

public class CompCast : MonoBehaviour
{
    public CompétenceClass[] playerComp;
    //public Component Spell1;

    public Text[] textComp;
    
    public GameObject ultEz; //Spell1
    public Camera cam;

    public GameObject shield;
    public float actualShield;
    public bool shilded;
    public float timerShield;


    public GameObject aOE;
    public Vector3 aoeRange;

    void Start()
    {
        cam = Camera.main;
    }

  
    void Update()
    {
        timerShield -= Time.deltaTime;
        
        if (Input.GetKeyDown("1"))
        {
            if (playerComp[0].cd <= 0)
            {
                UsedSpell(playerComp[0].id);
                playerComp[0].cd = playerComp[0].baseCd;
            }
        }
        
        if (Input.GetKeyDown("2"))
        {
            if (playerComp[1].cd <= 0)
            {
                UsedSpell(playerComp[1].id);
                playerComp[1].cd = playerComp[1].baseCd;
            }
        }
        
        if (Input.GetKeyDown("3"))
        {
            if (playerComp[2].cd <= 0)
            {
                UsedSpell(playerComp[2].id);
                playerComp[2].cd = playerComp[2].baseCd;
            }
        }

        foreach (var comp in playerComp)
        {
            if (comp.cd > 0)
            {
                comp.cd -= Time.deltaTime;
            }

            int i = (int) comp.cd;
            textComp[comp.id].text = i.ToString();
        }
        

        if (timerShield <= 0)
        {
            actualShield = 0;
        }
    }

    public void UsedSpell(int i)
    {
        switch (i)
        {
            case 0:
                RaycastHit hit;
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);

               // Vector3 mouseInWorld;
        
                if (Physics.Raycast(ray, out hit, 100))
                {
                   // mouseInWorld = hit.transform.position;
                }
        
                Vector3 tartgetPos = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                transform.LookAt(tartgetPos);
                var ez = Instantiate(ultEz, transform.position, transform.rotation);
                //ez.transform.eulerAngles = new Vector3(ez.transform.eulerAngles.x, ez.transform.eulerAngles.y, ez.transform.eulerAngles.z + 90);
                
                break;
            case 1: 
                print("shield");
                GameObject iShield = Instantiate(shield, transform.position, transform.rotation);
                iShield.transform.SetParent(gameObject.transform);
                actualShield += 100;
                timerShield = 1;
                break;
            case 2:
                print("AOE");
                Ray ray2 = cam.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray2, out hit, 100))
                {
                }
                
                Vector3 tartgetPos2 = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                var heading = tartgetPos2 - transform.position;
                Mathf.Clamp(heading.x, -aoeRange.x, aoeRange.x);
                Mathf.Clamp(heading.z, -aoeRange.z, aoeRange.z);
                Instantiate(aOE, heading, transform.rotation);
                break;
        }
    }
}
