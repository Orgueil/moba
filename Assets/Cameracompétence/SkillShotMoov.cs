﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillShotMoov : MonoBehaviour
{
    public float maxDistance;
    
    void Start()
    {
        
    }

    void Update()
    {
        if (transform.position.x >= maxDistance || transform.position.z >= maxDistance || transform.position.x <= -maxDistance || transform.position.z <= -maxDistance)
        {
            Destroy(gameObject);
        }
        
        transform.Translate(transform.forward, Space.World);
    }
}
