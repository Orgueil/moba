﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMoov : MonoBehaviour
{
    public float panSpeed;
       public float panLimits;
   
       public Vector2 panLimit;
   
       public float scrollSpeed;
   
       public float maxY = 120f;
       public float minY = 20f;
   
       public GameObject playerToFollow;
   
       public bool semiLocked;
       
       void Start()
       {
           
       }
   
   
       void Update()
       {
   
           Vector3 pos = transform.position;
           
           if (Input.GetKey(KeyCode.UpArrow) || Input.mousePosition.y >= Screen.height - panLimits)
           {
               pos.z += panSpeed * Time.deltaTime;
           }
           
           if (Input.GetKey(KeyCode.DownArrow) || Input.mousePosition.y <= panLimits)
           {
               pos.z -= panSpeed * Time.deltaTime;
           }
           
           if (Input.GetKey(KeyCode.LeftArrow) || Input.mousePosition.x <= panLimits)
           {
               pos.x -= panSpeed * Time.deltaTime;
           }
           
           if (Input.GetKey(KeyCode.RightArrow) || Input.mousePosition.x >= Screen.width - panLimits)
           {
               pos.x += panSpeed * Time.deltaTime;
           }
           
   
           float scroll = Input.GetAxis("Mouse ScrollWheel");
   
           pos.y -= scroll * scrollSpeed * 100f * Time.deltaTime;
   
           pos.y = Mathf.Clamp(pos.y, minY, maxY);
           pos.x = Mathf.Clamp(pos.x, -panLimit.x, panLimit.x);
           pos.z = Mathf.Clamp(pos.z, -panLimit.y, panLimit.y);
           
           if (Input.GetButton("Jump"))
           {
               if (semiLocked)
               {
                   pos.x = Mathf.Clamp(pos.x, playerToFollow.transform.position.x - 5, playerToFollow.transform.position.x + 5);
                   pos.z = Mathf.Clamp(pos.z, playerToFollow.transform.position.x - 5, playerToFollow.transform.position.x + 5);
               }
               else
               {
                   pos.x = playerToFollow.transform.position.x;
                   pos.z = playerToFollow.transform.position.z;
               }
           }
   
           transform.position = pos;
       }
}
