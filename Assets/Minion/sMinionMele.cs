﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sMinionMele : MonoBehaviour
{
    public float rayDistance;
    public float radius;
    public LayerMask layersEnemy;
    private Rigidbody rb;
    private bool attack;
    private int goldValue = 100;
    private int xpValue = 100;
    public int health;

    public void TakeDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Die();
        }
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;

        if (Physics.SphereCast(transform.position, radius, new Vector3(0, 0, 0), out hit, rayDistance))
        {

            attack = true;
            Debug.Log("Enemie détecter");
        }
        else
        {
            attack = false;
        }
        if (attack == true)
        {
            Debug.Log("Tire");
        }
    }

    void Die()
    {
        {
            butin.loot += goldValue;
            butin.xp += xpValue;
            Destroy(gameObject);
        }
    }
}
